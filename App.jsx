import './App.css'
import Header from './header'
import News from './news'

function App() {
  

  return (
    <>
      
     <Header/> 
      <main>
      <span id="name">The wall street journal</span>
      <div className='list'>
      <News/>
      <News/>
      <News/>
      </div>
     
      </main>
      <footer></footer>
    </>
  )
}

export default App
